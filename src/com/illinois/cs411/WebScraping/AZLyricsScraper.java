package com.illinois.cs411.WebScraping;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import com.illinois.cs411.Song.Song;

public class AZLyricsScraper implements LyricScraperIF {
	
	private String base = "http://www.azlyrics.com/lyrics/";

	public Song getLyrics(String artist, String song) {
		String url = base + artist.replaceAll("\\s+", "").toLowerCase() + "/" + song.replaceAll("\\s+", "").toLowerCase() + ".html";
		Song scrapedSong = new Song();
		scrapedSong.setArtistName(artist);
		scrapedSong.setSongName(song);

		Document doc = null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// String data
		String lyrics = doc.select("div").get(21).toString();
		lyrics = this.cleanLyricsHtml(lyrics);
		
		scrapedSong.setLyrics(lyrics);
		return scrapedSong;
	}
	
	public String cleanLyricsHtml(String lyrics)
	{
		/*
		lyrics = lyrics.replace("<br>", "");
		lyrics = lyrics.replace("</br>", "");
		lyrics = lyrics.replace("<div>", "");
		lyrics = lyrics.replace("</div>", "");
		lyrics = lyrics.replace("\n", "");
		lyrics = lyrics.replaceAll("<!--(.*?)-->", "");
		lyrics = lyrics.replaceAll("<i>(.*?)</i>", "");
		lyrics = lyrics.replaceAll("[(.*?)]", "");
		*/
		
		lyrics = lyrics.replaceAll("<[^>]*>", "");
		lyrics = lyrics.replace("\n", "");
		
		return lyrics;
	}

}
