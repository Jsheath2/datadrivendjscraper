package com.illinois.cs411.WebScraping;

import com.illinois.cs411.Song.Song;

public interface LyricScraperIF {
	
	public Song getLyrics(String arist, String song);

}
