package com.illinois.cs411.Song;

public class Song {
	
	private String songName;
	
	private String artistName;
	
	private String lyrics;
	
	public void setSongName(String songName)
	{
		this.songName = songName;
	}
	
	public void setArtistName(String artistName)
	{
		this.artistName = artistName;
	}
	
	public void setLyrics(String lyrics)
	{
		this.lyrics = lyrics;
	}

	public String getSongName() {
		return songName;
	}

	public String getArtistName() {
		return artistName;
	}

	public String getLyrics() {
		return lyrics;
	}
	

	

}
